package top.hugo.admin.mapper;


import top.hugo.admin.entity.SysUser;
import top.hugo.db.mapper.BaseMapperPlus;

/**
 * 用户表 数据层
 *
 * @author kuanghua
 */
public interface SysUserMapper extends BaseMapperPlus<SysUserMapper, SysUser, SysUser> {
}
