package top.hugo.admin.mapper;

import top.hugo.admin.entity.SysMenu;
import top.hugo.db.mapper.BaseMapperPlus;

/**
 * 菜单表 数据层
 *
 * @author kuanghua
 */
public interface SysMenuMapper extends BaseMapperPlus<SysMenuMapper, SysMenu, SysMenu> {
}
